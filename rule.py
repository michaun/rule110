# Copyright (C) 2024 Michal Niezborala <michaun _at_ protonmail [dot] com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import matplotlib.pyplot as plt
import tkinter as tk
from tkinter import filedialog

Width = 60
Height = 60
Rule = 110  # rule 110


class computation:
    def __init__(self, w, h, r):
        self.width = w
        self.height = h
        self.rule = r
        self.board = np.zeros((h, w), dtype=bool)
        self.isRunning = False
        self.GoLRule = True
        self.step = 0
        self.fig = None
        self.ax = None
        self.events = []

    def game_of_life(self):
        work = np.copy(self.board)  # work copy
        boardlen = len(work)
        linelen = len(work[0])
        for x in range(boardlen):
            for y in range(linelen):
                # sum up 3x3 field and subtract middle block
                N = (np.sum(work[y-1:y+2, x-1:x+2]) - work[y][x])
                match N:
                    case 2:
                        self.board[y][x] = work[y][x]
                    case 3:
                        self.board[y][x] = 1
                    case _:
                        self.board[y][x] = 0

    def process_rule(self, rule):
        current_line = self.board[self.step]
        next_line = self.board[self.step + 1]
        linelen = len(current_line) - 1
        for current_cell in range(1, linelen):
            for r in range(0, 7):
                if (
                        current_line[current_cell-1] == (r >> 2 & 1) and
                        current_line[current_cell] == (r >> 1 & 1) and
                        current_line[current_cell+1] == (r >> 0 & 1)):
                    next_line[current_cell] = (rule >> r & 1)

    def show(self):
        self.ax.cla()
        self.resetaxes()
        if self.height > 100 or self.width > 100:
            self.ax.pcolormesh(self.board, cmap='binary')
        else:
            self.ax.pcolormesh(self.board, cmap='binary', edgecolors='k')
        self.fig.canvas.draw_idle()
        self.fig.canvas.start_event_loop(0.01)

    def onkeypress(self, event):
        if event.key in ['S', 's']:
            root = tk.Tk()
            root.withdraw()
            file_path = filedialog.asksaveasfilename()
            np.savetxt(file_path, self.board)
        if event.key in ['O', 'o']:
            root = tk.Tk()
            root.withdraw()
            file_path = filedialog.askopenfilename()
            self.board = np.loadtxt(file_path)
            self.show()
        if event.key in ['P', 'p']:
            root = tk.Tk()
            root.withdraw()
            file_path = filedialog.asksaveasfilename()
            plt.savefig(file_path)

    def onclick(self, event):
        try:
            if event.button == 3:
                if event.dblclick is False:
                    self.isRunning = False
                    self.iterate()
                else:
                    self.isRunning = True
                    while self.isRunning is True:
                        repeated = np.copy(self.board)
                        self.iterate()
                        self.show()
                        if np.array_equal(repeated, self.board):
                            self.isRunning = False
                            np.delete(repeated)
            if event.button == 2:
                self.step = 0
                self.board.fill(0)
                if event.dblclick is False:
                    if self.GoLRule:
                        self.board = np.random.randint(2,
                                                       size=(self.height,
                                                             self.width))
                    else:
                        self.board[0] = np.random.randint(2,
                                                          size=(1,
                                                                self.width))
            if event.button == 1:
                if event.dblclick is False:
                    x = int(event.xdata)
                    y = int(event.ydata)
                    self.board[y][x] = 0 if (self.board[y][x] == 1) else 1
                else:
                    self.GoLRule = not self.GoLRule
            self.show()
            print('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
                  ('double' if event.dblclick else 'single', event.button,
                   event.x, event.y, event.xdata, event.ydata))
        except TypeError:
            pass
        except RuntimeError:
            pass

    def on_close(self, event):
        self.isRunning = False
        self.fig.clf()
        self.fig.canvas.stop_event_loop()
        for ev in self.events:
            self.fig.canvas.mpl_disconnect(ev)
        plt.close('all')

    def iterate(self):
        try:
            if self.GoLRule:
                self.game_of_life()
            else:
                self.process_rule(self.rule)
            self.step += 1
        except IndexError:
            pass

    def pltinit(self):
        plt.rcParams['toolbar'] = 'None'
        self.fig, self.ax = plt.subplots()
        self.resetaxes()
        self.events.append(self.fig.canvas.mpl_connect('button_press_event',
                                                       self.onclick))
        self.events.append(self.fig.canvas.mpl_connect('key_press_event',
                                                       self.onkeypress))
        self.events.append(self.fig.canvas.mpl_connect('close_event',
                                                       self.on_close))

    def resetaxes(self):
        t = "Left: Toggle (single) field (double) Algo    S:save\n"
        t += "Middle: (Single) random (double) Clear    O: open\n"
        t += "Right: (single) Step/Stop (double) Go    P: Save as pdf"
        self.ax.set_title(t)
        self.ax.set_xticks([])
        self.ax.set_yticks([])
        self.ax.invert_yaxis()
        self.ax.set_aspect('equal')


z = computation(Width, Height, Rule)
z.pltinit()
plt.show()
